#!/bin/bash
set -eu
source ./env.default

SCALE=${1:-1}
docker run --rm "$START_APP_IMAGE" $SCALE
