<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8" import="com.example.Dog"%>
<html>

<body>
    <h2>Dog of the day</h2>
    <%! String dog;  %>
    <%
    Dog dogBuilder = new Dog();
    dog = dogBuilder.GenDog(1,true);
%>
    <p style="font-family: courier; white-space: pre"><%= dog %></p>
</body>

</html>