FROM jboss/wildfly:17.0.1.Final

COPY target/dogwar-1.0.war /opt/jboss/wildfly/standalone/deployments/dog.war
EXPOSE 8080