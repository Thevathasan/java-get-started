FROM maven:3.6.1-jdk-11-slim AS maven
COPY . .
RUN ./build.sh

FROM tomcat:9.0.24-jdk11-openjdk-slim
COPY --from=maven /target/dogwar-1.0.war /usr/local/tomcat/webapps/dog.war
EXPOSE 8888