#!/bin/sh
set -eu
rm -rf target
# build binaries (war, jar, etc)
docker-compose run --user $(id -u):$(id -g) dog-build
# build docker image
docker-compose build dog